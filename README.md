# Behavioral State Space Reconstruction

This repository contains the main scripts for state space reconstruction and subsequent analysis described in:

Ahamed T, Costa AC, Stephens GJ (2019) "Capturing the Continuous Complexity of Behavior in C. elegans" https://www.biorxiv.org/content/10.1101/827535v1

Please forward any bugs, comments, questions and suggestions to tahamed@lunenfeld.ca or tosifahamed@gmail.com

## -------------------------------------------------------------------------------------------

The main folder contains two files

lorenz_embed.m: Embedding example for the Lorenz system

worm_embed.m: Embedding for a single worm, from choosing reconstruction parameters to periodic orbits.

## -------------------------------------------------------------------------------------------

All the scripts are contained in 'lib' folder, with the most important being

embed_cells.m: Performs delay embedding on an observation time series.

predict_nn_all.m: Implements nearest neighbor predictor and estimates Tpred and related quantities.

learn_jac.m: Estimates the 1-step Jacobian matrix in a given state space.

perorb_find_adapt.m: Finds periodic orbits using close recurrences.

## -------------------------------------------------------------------------------------------

The 'data' folder contains

1) 'crawl.mat': 5 eigenworm coefficients for all 12 worms used in the analysis. See [1-2] and references therein for detailed description of these datasets.

2) 'escape.mat': 5 eigenworm coefficients for all escape response sequences used in the analysis. See [1-2] and references therein for detailed description of these datasets.

3) 'roaming.mat': 5 eigenworm coefficients for roaming worms.

4) 'dwelling.mat': 5 eigenworm coefficients for all dwelling worms.

5) 'npr1.mat': 5 eigenworm coefficients for all npr1 mutants.

6) 'EigenWorms.mat': Matrix containing all the eigenworms, used to transform back and forth frin the space of tangential angles.

7) 'mds_all.mat': Ensemble behavioral modes for 6,7 and 8 dimensions for different K and m

8) 'mds_w7.mat': Behavioral modes for the example worm used in the manuscript in 6,7 and 8 dimensions.

[1] - Broekmans O, Rodgers J, Ruy S, Stephens GJ (2016) "Resolving coiled shapes reveals new reorientation behaviors in C. elegans" eLife 2016;5:e17227; https://doi.org/10.7554/elife.17227

[2] - Broekmans OD, Rodgers JB, Ryu WS, Stephens GJ (2016) Data from: Resolving coiled shapes reveals new reorientation behaviors in C. elegans. Dryad Digital Repository. https://doi.org/10.5061/dryad.t0m6p