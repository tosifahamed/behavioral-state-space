%% 
addpath lib/
addpath lib/dyncode

%% Simulate Lorenz system
ic=[0.85957 1.7318 1.2865];
dt=0.01;
opts = odeset('RelTol',1e-8,'AbsTol',1e-8);
[t,s]=ode45('lorenz',0:dt:400,ic,opts);
s=s(1000:end,:); %remove the transient
%nse=0.005;
nse=0.00;
y=s(:,1)+randn(length(s),1)*std(s(:,1))*nse; %make an observation of the first coordinate with 0.5% noise
y=bsxfun(@minus,y,mean(y));
%%
clear rmsp_d es_d ar_d del_d ts_d rmsp es ar del ts
%params
maxdel=90;
npred=5000;
Tmax=700;
mmax=8;
delrange=1:maxdel;

%generate random test indices for the search
[ybar,~,idx,~]=embed_cells(y,maxdel); 
%center ybar by removing mean
ybar=bsxfun(@minus,ybar,mean(ybar));

%%Now we find the number of nearest neighbors.
%%Followsing is a heuristic approach, where the in each embedding dimension
%%number of nearest neighbors are found by minimizing the 0-step prediction
%%error. After plotting, choose a value of Nb when it is independent of
%%embedding dimension. Following code takes time, so I have commented it out
%%and I am using a precomputed value for Nb.

% for i=1:40
%      nb(i)=find_nb(ybar(:,1:i));disp(i);
% end
% plot(nb,'o-');xlabel('Embedding Dimension');ylabel('Number of Nearest Neighbors');

nb=10;
idx=idx(1:end-Tmax,1);
testinds=idx(randi(length(idx),npred*2,1));
%%
for deli=1:length(delrange)
    [ybar,ybar_orig,ybar_orig_idx,sten]=embed_cells(y,delrange(deli)); 
    %get test indices that are included in the embedding   
    [~,tstidx] = ismember(testinds,ybar_orig_idx);    
    %rmsp=predict_nn_all(ybar,ybar_orig,Tmax,npred,1,nb,tstidx,[1 length(ybar)]);
    [rmsp_k{deli},es_k(deli,:),ar_k(deli,:),tpred_k(deli,:),ts_k(deli,:)]=predict_nn_all(ybar,ybar_orig,Tmax,npred,1,nb,tstidx,sten);
    
    disp([delrange(deli)]);
end
errorbar(delrange,tpred_k(:,1),tpred_k(:,1)-tpred_k(:,2),tpred_k(:,3)-tpred_k(:,1),'ko','markersize',8);
%%
Kstar=25;
[ybar,ybar_orig,ybar_orig_idx]=embed_cells(y,Kstar); 
[~,tstidx] = ismember(testinds,ybar_orig_idx);
[U,S,V]=svd(ybar,'econ');
for mi=1:mmax  
   [rmsp_m{mi},es_m(mi,:),ar_m(mi,:),tpred_m(mi,:),ts_m(mi,:)]=predict_nn_all(U(:,1:mi),ybar_orig,Tmax,npred,V(:,1:mi)*S(1:mi,1:mi),nb,tstidx,sten);
   disp(mi);
end
errorbar(1:mmax,tpred_m(:,1),tpred_m(:,1)-tpred_m(:,2),tpred_m(:,3)-tpred_m(:,1),'ko','markersize',8);

%% Display embedding
Kstar=25;
mstar=3;
ybar=embed_cells(y,Kstar); 
[U,S,V]=svd(ybar,'econ');
figure;plot3(U(:,1),U(:,2),U(:,3));