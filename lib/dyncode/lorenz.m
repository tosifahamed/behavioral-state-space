%%The lorenz ode description with canonical parameters
function [f] = lorenz(t,x)

s=10;
r=28;
b=8/3;

%some other parameter sets
% s=16;
% r=40;
% b=4;

%
%s=16;
%r=45.92;
%b=4;

f = [s*(x(2)-x(1)); r*x(1)-x(2)-x(1)*x(3); x(1)*x(2)-b*x(3);];
end