%%The Rossler ode description
function [f] = rossler(t,x)

    %commonly studied parameters
    %a=0.1;
    %b=0.1;
    %c=14;

    %original parameters by rossler
    a=0.2;
    b=0.2;
    c=5.7;

    f = [-x(2)-x(3); x(1)+a*x(2); b+x(3)*(x(1)-c);];
end

