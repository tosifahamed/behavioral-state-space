%% 
addpath data/
addpath lib/
addpath lib/FastICA_25/
load('crawl.mat')

%% observe
wid=7;
y=tr{wid}(:,1:5);clear tr wid;
ts=16; %sampling rate

%%
clear rmsp_k es_k ar_k tpred_k ts_k rmsp_m es_m ar_m tpred_m ts_m
%params
%Kmax, maximum K used in the search. As a first guess, autocorrelation
%time, zero of autocorrelation function,oscillation period or its double.
%Delrange can be used to sample K coarsely, recommended when starting.
maxdel=80;delrange=1:2:maxdel;
%number of random points used for prediction. Larger number of points give
%a better estimate, but takes longer.
npred=7000; 
%tau_max. Harder to estimate, set it to several multiples of the
%autocorrelation function. Check and error curves in rmsp_d and verify that
%the error has saturated. If it never saturates then most likely the
%nonstationarity is too strong. Contact tosifahamed@gmail.com in this case
Tmax=700; 


%%generate random test indices for the search
[ybar,~,idx,~]=embed_cells(y,maxdel);  %embed for maximum delay to calculate the valid range of test indices
%%center ybar by removing mean
ybar=bsxfun(@minus,ybar,mean(ybar));

%%Now we find the number of nearest neighbors.
%%Followsing is a heuristic approach, where the in each embedding dimension
%%number of nearest neighbors are found by minimizing the 0-step prediction
%%error. After plotting, choose a value of Nb when it is independent of
%%embedding dimension. Following code takes time, so I have commented it out
%%and I am using a precomputed value for Nb.

% for i=1:40
%     nb(i)=find_nb(ybar(:,1:i));disp(i);
% end
% plot(nb,'o-');xlabel('Embedding Dimension');ylabel('Number of Nearest Neighbors');
% 
% %pick the last entry in nb array as number of nearest neighbors
% %and use that for the entire search process below.
% nb=nb(end);

%getting valid test indices
nb=6;
idx=idx(1:end-Tmax,1); 
testinds=idx(randi(length(idx),npred*2,1));
%%
%search for optimal delay K*
for deli=1:length(delrange)    
    [ybar,ybar_orig,ybar_orig_idx,sten]=embed_cells(y,delrange(deli)); 
    %get test indices that are included in the embedding
    [~,tstidx] = ismember(testinds,ybar_orig_idx);
    [rmsp_k{deli},es_k(deli,:),ar_k(deli,:),tpred_k(deli,:),ts_k(deli,:)]=predict_nn_all(ybar,ybar_orig,Tmax,npred,1,nb,tstidx,sten);
    
    disp([delrange(deli)]);
end
%plot
figure;errorbar(delrange,tpred_k(:,1),tpred_k(:,1)-tpred_k(:,2),tpred_k(:,3)-tpred_k(:,1),'ko','markersize',8);
%% Search for optimal embedding dimension m*
%set the value of Kstar as determined from Tpred(K) above
%Maximum embedding dimension corresponds to the maximum number of singular
%vectors to consider. 
mmax=14;

%setting to precomputed value
Kstar=10;
[ybar,ybar_orig,ybar_orig_idx]=embed_cells(y,Kstar); 
[~,tstidx] = ismember(testinds,ybar_orig_idx);
%perform SVD
[U,S,V]=svd(ybar,'econ');
for mi=1:mmax
    [rmsp_m{mi},es_m(mi,:),ar_m(mi,:),tpred_m(mi,:),ts_m(mi,:)]=predict_nn_all(U(:,1:mi),ybar_orig,Tmax,npred,V(:,1:mi)*S(1:mi,1:mi),nb,tstidx,sten);
    disp(mi);
end
figure;errorbar(1:mmax,tpred_m(:,1),tpred_m(:,1)-tpred_m(:,2),tpred_m(:,3)-tpred_m(:,1),'ko','markersize',8);

%% Use SVD or ICA to get the modes. The modes will depend on the data and situation. Even on the same data ICA modes can be slightly different each time.
Kstar=12;
mstar=7;
[ybar,ybar_orig,ybar_orig_idx]=embed_cells(y,Kstar); 
%center ybar by removing mean
ybar=bsxfun(@minus,ybar,mean(ybar));

%perform svd or ica decomposition 
%[U,S,V]=svd(ybar,'econ');
%[icasig,~,~]=doica(ybar',mstar);icasig=icasig';

%ICA modes are qualitatively same across different runs, but there are some
%differences from run to run in the details.
%For consistency and repeatability I have already saved some modes, simply load those and project.
%If you are are using other modes then comment out the first two lines.
load mds_w7.mat
W=w7mds(2).W;
xx=ybar*W';

%%Show the phase space projections on different modes
cpos=[15.8106; 83.3113;18.0871];
len=floor(size(xx,1)/2);
figure;
subplot(1,3,1);plot(xx(end-len:end,3),xx(end-len:end,4));
subplot(1,3,2);plot(xx(end-len:end,6),xx(end-len:end,7));
subplot(1,3,3);plot3(xx(end-len:end,1),xx(end-len:end,2),xx(end-len:end,5));campos(cpos);
%% Estimate The Jacobians and Lyapunov Spectrum
[merr,Jac_data]=learn_jac(xx);
figure;plot(Jac_data.lexp_avg*ts);

%% Get periodic orbits and plot all orbits of period 1
[pertab,pers]=perorb_find_adapt(xx);
figure
clf;tpper=pertab(pertab(:,6)==1,:);
plot_clusters_traj(xx,tpper,[3 4 2;6 7 2;1 2 5],ones(12,1),[1 1 1]*0.2)
%%
clear local_lam
figure;subplot(1,2,1);
plot((1:5:300)/pers(1),histc(pertab(:,4),1:5:300),'linewidth',2);
xlabel('$\frac{p}{p_{min}}$','interpreter','latex','fontsize',18);ylabel('No. of UPOs');
subplot(1,2,2);
%get local jacobians and floquet exponents
jj=Jac_data.Amat(2:end,:,:);
parfor i=1:size(pertab,1)
    [local_lam(i,:)] = local_lyap_alt(jj(:,:,pertab(i,1):pertab(i,1)+pertab(i,4)-1));
end
subplot(1,2,2);
histogram(local_lam(:,1)*ts,25,'normalization','pdf','edgecolor','none')
xlim([0 1.8]);
xlabel('Floquet Exponent (1/s)');ylabel('PDF');